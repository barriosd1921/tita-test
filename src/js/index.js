import '../styles/styles.css';
import './clics';

import item_1 from '../images/item_card_1.jpg';
import item_2 from '../images/item_card_2.jpg';
import item_3 from '../images/item_card_3.jpg';
import item_4 from '../images/item_card_4.jpg';

const ArrayCards = [
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Branding",
        img: item_1,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "Web",
        img: item_2,
        size: 'tall'
    },
    {
        name: "Creative Portofilio",
        brand: "Photography",
        img: item_3,
        size: 'small'
    },
    {
        name: "Creative Logo",
        brand: "App",
        img: item_4,
        size: 'tall'
    },

];

const loadCards = () => {
    let count = 1;

    ArrayCards.map( card => {

        if(count > 3)
            count = 1;

        const element = document.querySelector(`#col_${count}`);

        element.insertAdjacentHTML('beforeend', `
            <div class="card-${card.size} mb-2">
                <img 
                    src="${card.img}" 
                    alt="Test"
                    class="w-100 h-100"
                >
                <div class="px-3 text-center">
                    <h3 class="pb-1 text-uppercase">${card.name}</h3>
                    <p class="mt-1">${card.brand}</p>
                </div>
            </div>
        `);

        count ++;    
    });
}

(() => {
    loadCards();

})()