const elements =  document.querySelectorAll('[toggle-bar]');
const showMore = document.querySelector('#more');
const ourWork = document.querySelector('#work');

const handleClick =  e => {
    const Attr = document.querySelector('[toggle-bar]').getAttribute('toggle-bar');

    const list = document.querySelector(Attr);

    list.classList.toggle('in');
}

const handleMore = e => {
    let property = getComputedStyle(document.body).getPropertyValue('--height-portfolio');
    let propertyMv = getComputedStyle(document.body).getPropertyValue('--height-portfolio-mv');
    property = parseInt(property.split('vh')[0])+100;
    propertyMv = parseInt(propertyMv.split('vh')[0])+100;
    document.documentElement.style.setProperty('--height-portfolio', `${property}vh`);
    document.documentElement.style.setProperty('--height-portfolio-mv', `${propertyMv}vh`);
}

const handleWork = e => {
    const section = document.getElementById("portfolio");

    section.scrollIntoView({
        behavior: "smooth"
    });
}

elements.forEach( element => {    
    element.addEventListener("click", handleClick);
});

showMore.addEventListener("click", handleMore);

ourWork.addEventListener("click", handleWork);