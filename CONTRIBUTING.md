## Guia para generacion de nuevas ramas y commits

### GENERACION DE RAMAS

Para la creación de nuevas ramas se aplicara la nomenclatura siguiente: 

```
    <sufijo>-<nombre identificador>
```

Donde `sufijo` sera el disminutivo de la accion realizada en la rama, a continuación se desgloza las posibles opciones:

* **build**: Rama para cambios que afectan el sistema de compilación o dependencias externas.
* **ci**: Rama destinada solo a cambios en archivos de configuración.
* **docs**: Rama para solo cambios en documentacion.
* **feat**: Rama para nuevas caracteristicas.
* **fix**: Rama para solventar errores.
* **perf**: Rama para cambios que mejoran el rendimiento de la aplicación.
* **refactor**: Rama para cambios que no corrige ningun error ni mejora el rendimiento.
* **style**: Rama para cambios que no afectan el significado del codigo. Ej. Espacios, Comas, Puntos.
* **test**: Rama para agregar pruebas faltantes o corregir pruebas existentes.

Algunos ejemplos para esto podrian ser:

* feat-admin-search
* docs-veronica
* fix-events-google

### CREACION DE COMMITS

Para crear commits a una rama previamente creada se recomienda:

1. Resumir brevemente los cambios realizados.
2. NO usar más de 50 caracteres en el asunto del commit, en caso de ser necesario usar un limite maximo de 72 caracteres.
3. Iniciar el mensaje con un `sufijo` previamente desglozados, seguido de dos puntos ':' y un espacio ' '.
4. Hacer uso del ingles para crear los mensajes.
5. NO terminar los mensajes de commit con un punto '.'
6. Hacer uso de verbos imperativos, por ejemplo: `Clean your room`, `Close the door`, `Take out the trash`.

Varios ejemplos de commits podrian ser:

```
* fix: Fix errors in the Tranfer component
* style: Change code in scene dashboard
```
