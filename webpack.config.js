const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const webpackConfig = {
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './js/app.js'
    },
    entry: {
        main: './src/js/index.js'
    },
    devServer: {
        port: 4000,
    },
    devtool: "inline-source-map",
    module: {
        rules: [
            { 
                test: /\.js$/, 
                exclude: /node_modules/, 
                loader: "babel-loader" 
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: { 
                        "presets": [[
                            "@babel/preset-env",
                            {
                                "targets": {
                                    "browsers": [
                                        ">=1%",
                                        "not ie 11",
                                        "not op_mini all"
                                    ]
                                },
                                "debug": true
                            }
                      ]] 
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, 
                    'css-loader'
                ],
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
           }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ 
            filename: 'index.html',
            template: 'src/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: './css/styles.css'
        }),
        new CopyPlugin({
            patterns: [
                { from: 'src/styles/styles.css.map', to: '../dist/css' },
                { from: 'src/images', to: '../dist/images' },
            ],
        })
    ]
}

module.exports = webpackConfig;